# Blocklens

Popular blockchain network metrics web application with a dinamic modern
interface.

---

## Network support

[ ] Ethereum (ETH)

[ ] Bitcoin (BTC)

[ ] Binance Smart Chain (BSC)

[ ] Polkadot (DOT)

[ ] Solana (SOL)

[ ] Cardano (ADA)

---

## Data support

**Network Data**:

- Network Hash Rate
- Difficulty
- Total Supply of Tokens/Cryptocurrency
- Number of Active Nodes
- Average Block Time

**Block Data**:

- Block Hash
- Block Height
- Timestamp
- Miner Address
- Number of Transactions in the Block
- Block Size

**Transaction Data**:

- Transaction Hash
- Sender and Receiver Addresses
- Transaction Amount
- Timestamp
- Gas Limit and Gas Used
- Transaction Status (Success/Failure)

**Address Data**:

- Balance
- Transaction History
- Tokens Held (for token-based blockchains)
- Associated Smart Contracts (if applicable)

**Token Data** (for token-based blockchains):

- Total Supply
- Circulating Supply
- Token Holders
- Token Transfers
- Token Balances

**Consensus Data**:

- Consensus Algorithm Used
- Validator Nodes
- Block Finality

**Smart Contract Data**:

- Contract Address
- Contract Creator
- Contract Code
- Transactions related to the Contract
- Internal Transactions
- Events emitted by the Contract

**Miscellaneous Data**:

- Network Health Metrics (e.g., latency, throughput)
- Node Health Metrics
- Pending Transactions
