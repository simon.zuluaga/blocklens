import { useEffect, useState } from "react"
import { ethers } from "ethers"

type ProviderState = ethers.EtherscanProvider | null

function App() {
  const [provider, setProvider] = useState<ProviderState>(null)
  const [blockNumber, setBlockNumber] = useState<number>(
    Number.NEGATIVE_INFINITY
  )

  const loadBlockchainData = async () => {
    const network = "homestead"
    const apiKey = "CJIE7VZAHX1H1G7YTCD13733XKI5US2CXS"
    const provider = new ethers.EtherscanProvider(network, apiKey)
    setProvider(provider)
  }

  const getBlockNumber = async () => {
    if (!provider) return

    const blockNumber = await provider.getBlockNumber()
    setBlockNumber(blockNumber)
  }

  useEffect(() => {
    loadBlockchainData()
  }, [])

  return (
    <div>
      <h1>Provider test</h1>
      <div>
        <button onClick={() => getBlockNumber()}>
          {blockNumber === Number.NEGATIVE_INFINITY
            ? "Get block number"
            : `Latest: ${blockNumber}`}
        </button>
      </div>
    </div>
  )
}

export default App
